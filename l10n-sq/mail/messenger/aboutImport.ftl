# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importim

## Header

import-from-app = Importo prej Aplikacioni

export-profile = Eksportim

## Buttons

button-back = Mbrapsht

button-continue = Vazhdo

button-export = Eksporto

## Import from app steps

app-name-thunderbird = Thunderbird

app-name-seamonkey = SeaMonkey

app-name-outlook = Outlook

app-name-becky = Becky! Internet Mail

app-name-apple-mail = Apple Mail

## Import from file selections

## Import from app profile steps

items-pane-checkbox-accounts = Llogari dhe Rregullime

items-pane-checkbox-address-books = Libra Adresash

items-pane-checkbox-calendars = Kalendarë

items-pane-checkbox-mail-messages = Mesazhe Poste

## Import from address book file steps

addr-book-csv-file = Kartelë e ndarë me presje, ose me simbol tabulacioni (.csv, .tsv)

addr-book-ldif-file = Kartelë LDIF (.ldif)

addr-book-vcard-file = Kartelë vCard (.vcf, .vcard)

addr-book-sqlite-file = Kartelë baze të dhënash SQLite (.sqlite)

addr-book-mab-file = Kartelë baze të dhënash Mork (.mab)

addr-book-file-picker = Përzgjidhni një kartelë libri adresash

addr-book-csv-field-map-title = Përputh emra fushash

addr-book-csv-field-map-desc = Përzgjidhni fusha libri adresash që u përgjigjen fushave të burimit. Hiquni shenjën fushave që nuk doni të importohen.

addr-book-directories-pane-source = Kartelë burim:

## Import from address book file steps

## Import from calendar file steps

import-from-calendar-file-desc = Përzgjidhni kartelën iCalendar (.ics) që doni të importohet.

calendar-items-loading = Po ngarkohen zëra…

calendar-items-filter-input =
    .placeholder = Filtroni objekte…

calendar-select-all-items = Përzgjidhi krejt

calendar-deselect-all-items = Shpërzgjidhi krejt

## Import dialog

error-pane-title = Gabim

error-message-failed = Importimi dështoi papritmas, më tepër hollësi mund të ketë te Konsola e Gabimeve.

error-failed-to-parse-ics-file = Te kartela s’u gjetën zëra të importueshëm.

error-export-failed = Eksportimi dështoi papritmas, më tepër hollësi mund të ketë te Konsola e Gabimeve.

## <csv-field-map> element

csv-first-row-contains-headers = Rreshti i parë përmban emra fushe

csv-source-field = Fushë burimi

csv-source-first-record = Zëri i parë

csv-source-second-record = Zëri i dytë

csv-target-field = Fushë libri adresash

## Export tab

export-open-profile-folder = Hapni dosje profili

export-brand-name = { -brand-product-name }

## Summary pane

## Footer area

## Step navigation on top of the wizard pages

