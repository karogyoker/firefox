# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Dalej
# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = OK

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Przełączaj się między urządzeniami za pomocą odbierania kart
callout-firefox-view-tab-pickup-subtitle = Szybko odbieraj otwarte karty z telefonu i otwieraj je tutaj, aby nie tracić ani sekundy.
callout-firefox-view-recently-closed-title = Odzyskaj zamknięte karty w mgnieniu oka
callout-firefox-view-recently-closed-subtitle = Tutaj w magiczny sposób pojawią się wszystkie zamknięte karty. Nie musisz się już martwić, że przypadkowo zamkniesz jakąś stronę.
callout-firefox-view-colorways-title = Dodaj odrobinę koloru
# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Wybierz odcień kolorystyki, który do Ciebie pasuje. Tylko w przeglądarce { -brand-product-name }.
callout-firefox-view-colorways-reminder-title = Poznaj nasze najnowsze kolorystyki
# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Pokoloruj swoją przeglądarkę odcieniami zainspirowanymi przez niezależne głosy. Tylko w przeglądarce { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Przyspiesz przeglądanie za pomocą odbierania kart
continuous-onboarding-firefox-view-tab-pickup-subtitle = Korzystaj z otwartych kart na każdym urządzeniu, a także synchronizuj zakładki, hasła i nie tylko.
continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Zacznij teraz
