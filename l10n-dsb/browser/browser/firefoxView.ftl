# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-button-firefox-view =
    .label = { -firefoxview-brand-name }
    .tooltiptext = { -firefoxview-brand-name }

menu-tools-firefox-view =
    .label = { -firefoxview-brand-name }
    .accesskey = F

firefoxview-page-title = { -firefoxview-brand-name }

firefoxview-close-button =
    .title = Zacyniś
    .aria-label = Zacyniś

# Used instead of the localized relative time when a timestamp is within a minute or so of now
firefoxview-just-now-timestamp = Rowno

# This is a headline for an area in the product where users can resume and re-open tabs they have previously viewed on other devices.
firefoxview-tabpickup-header = Synchronizěrowane rejtariki
firefoxview-tabpickup-description = Wócyńśo boki z drugich rědow.

firefoxview-tabpickup-recenttabs-description = How se lisćina nejnowšych rejtarikow pokažo

# Variables:
#  $percentValue (Number): the percentage value for setup completion
firefoxview-tabpickup-progress-label = { $percentValue } % dokóńcone

firefoxview-tabpickup-step-signin-header = Pśejźćo njepósrědnje mjazy rědami
firefoxview-tabpickup-step-signin-description = Aby how rejtariki swójogo telefona dostał, pśizjawśo se nejpjerwjej abo załožćo konto.
firefoxview-tabpickup-step-signin-primarybutton = Dalej

firefoxview-tabpickup-adddevice-header = Synchronizěrujśo { -brand-product-name } na swójom telefonje abo tableśe
firefoxview-tabpickup-adddevice-description = Ześěgniśo { -brand-product-name } za mobilne rědy a pśizjawśo se tam.
firefoxview-tabpickup-adddevice-learn-how = Zgóńśo kak
firefoxview-tabpickup-adddevice-primarybutton = { -brand-product-name } za mobilny rěd wobstaraś

firefoxview-tabpickup-synctabs-header = Synchronizaciju rejtarikow zmóžniś
firefoxview-tabpickup-synctabs-description = { -brand-short-name } dowóliś, rejtariki mjazy rědami źěliś.
firefoxview-tabpickup-synctabs-learn-how = Zgóńśo kak
firefoxview-tabpickup-synctabs-primarybutton = Wócynjone rejtariki synchronizěrowaś

firefoxview-tabpickup-fxa-admin-disabled-header = Waša organizacija jo znjemóžniła synchronizaciju
firefoxview-tabpickup-fxa-admin-disabled-description = { -brand-short-name } njamóžo rejtariki mjazy rědami synchronizěrowaś, dokulaž waš administrator jo znjemóžnił synchronizěrowanje.

firefoxview-tabpickup-network-offline-header = Pśeglědujśo swój internetny zwisk
firefoxview-tabpickup-network-offline-description = Jolic wognjowu murju abo proksy wužywaśo, pśeglědujśo, lěc { -brand-short-name } ma pšawo na pśistup k internetoju.
firefoxview-tabpickup-network-offline-primarybutton = Hyšći raz wopytaś

firefoxview-tabpickup-sync-error-header = Mamy problemy ze synchronizaciju
firefoxview-tabpickup-generic-sync-error-description = { -brand-short-name } njamóžo tuchylu synchronizěrowańsku słužbu dojśpiś. Wopytajśo za někotare wokognuśa hyšći raz.
firefoxview-tabpickup-sync-error-primarybutton = Hyšći raz wopytaś

firefoxview-tabpickup-sync-disconnected-header = Zmóžniśo synchronizaciju, aby pókšacował
firefoxview-tabpickup-sync-disconnected-description = Za pśistup k swójim rejtarikam musyśo synchronizaciju w { -brand-short-name } dowóliś.
firefoxview-tabpickup-sync-disconnected-primarybutton = Synchronizaciju w nastajenjach zmóžniś

firefoxview-tabpickup-syncing = Wobcakajśo, mjaztym až se waše rejtariki synchronizěruju. Buźo jano wokognuśe traś.

firefoxview-mobile-promo-header = Wobstarajśo se rejtariki ze swójogo telefona abo tableta
firefoxview-mobile-promo-description = Aby se swóje nejnowše mobilne rejtariki woglědował, přśzjawśo se pla { -brand-product-name } na iOS abo Android.
firefoxview-mobile-promo-primarybutton = { -brand-product-name } za mobilny rěd wobstaraś

firefoxview-mobile-confirmation-header = 🎉 Wšykno gótowe!
firefoxview-mobile-confirmation-description = Něnto móžośo swóje rejtariki { -brand-product-name } ze swójogo tableta abo telefona wobstaraś.

firefoxview-closed-tabs-title = Njedawno zacynjone

firefoxview-closed-tabs-description = Wócyńśo boki znowego, kótarež sćo zacynił na toś tom rěźe.

firefoxview-closed-tabs-description2 = Wócyńśo boki znowego, kótarež sćo zacynił w toś tom woknje.
firefoxview-closed-tabs-placeholder = <strong>Žedne njedawno zacynjone boki</strong><br/>Njebójśo se nigda zacynjeny rejtarik zasej zgubiś. Móžośo jen pśecej how zasej dostaś.

# refers to the last tab that was used
firefoxview-pickup-tabs-badge = Slědny raz aktiwny

# Variables:
#   $targetURI (string) - URL that will be opened in the new tab
firefoxview-tabs-list-tab-button =
    .title = { $targetURI } w nowem rejtariku wócyniś

firefoxview-try-colorways-button = Barwowe kombinacije wopytowaś
firefoxview-no-current-colorway-collection = Nowe barwowe kombinacije se pśigótuju
firefoxview-change-colorway-button = Barwowu kombinaciju změniś

# Variables:
#  $intensity (String): Colorway intensity
#  $collection (String): Colorway Collection name
firefoxview-colorway-description = { $intensity } · { $collection }

firefoxview-synced-tabs-placeholder = <strong>Hyšći njejo nic wiźeś</strong><br/>Pśiducy raz, gaž bok w { -brand-product-name } na drugem rěźe wócynjaśo, wobstarajśo jen how na magiski nałog.

firefoxview-collapse-button-show =
    .title = Lisćinu pokazaś

firefoxview-collapse-button-hide =
    .title = Lisćinu schowaś
