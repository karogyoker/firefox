# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Neste

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Skjønar.

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Byt mellom einingar med synkroniserte faner

callout-firefox-view-tab-pickup-subtitle = Hent snøgt opne faner frå telefonen og opne dei her.

callout-firefox-view-recently-closed-title = Få tilbake dei attlatne fanene på ein augneblink

callout-firefox-view-recently-closed-subtitle = Alle dei attlatne fanene dine vil visast her. Du treng aldri å bekymre deg for å late att ein nettstad ved eit uhell igjen.

callout-firefox-view-colorways-title = Legg til ein fargeklatt

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Vel nyansen som snakkar til deg med fargesamansetjingar. Berre i { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Utforsk dei nyaste fargesamansetjingane våre

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Farg nettlesaren din med desse ikoniske nyansane, inspirert av uavhengige stemmer. Berre i { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Gjer nettopplevinga di betre med synkronisering av faner

continuous-onboarding-firefox-view-tab-pickup-subtitle = Få tilgang til dei opne fanene dine frå kva som helst eining. Synkroniser også bokmerke og passord, med meir.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Kom i gang
