# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Tiếp

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Đã hiểu!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Chuyển công việc giữa các thiết bị với tính năng Các thẻ trên thiết bị khác

callout-firefox-view-tab-pickup-subtitle = Nhanh chóng lấy các thẻ đang mở từ điện thoại của bạn và mở chúng ở đây để có hiệu suất công việc của bạn tối đa.

callout-firefox-view-recently-closed-title = Lấy lại các thẻ đã đóng của bạn trong tích tắc

callout-firefox-view-recently-closed-subtitle = Tất cả các thẻ đã đóng của bạn sẽ hiển thị ở đây một cách kỳ diệu. Không bao giờ lo lắng về việc vô tình đóng một trang web nữa.

callout-firefox-view-colorways-title = Thêm một chút màu sắc

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Chọn một đường màu phù hợp với phong cách của bạn. Chỉ trong { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Khám phá các đường màu mới nhất của chúng tôi

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Tô màu trình duyệt của bạn với những sắc thái mang tính biểu tượng này, lấy cảm hứng từ những tiếng nói độc lập. Chỉ có trong { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Tăng cường duyệt web của bạn với tính năng nhận thẻ từ thiết bị khác

continuous-onboarding-firefox-view-tab-pickup-subtitle = Truy cập các thẻ đang mở của bạn từ bất kỳ thiết bị nào. Đồng bộ hóa dấu trang, mật khẩu của bạn và hơn thế nữa.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Bắt đầu
