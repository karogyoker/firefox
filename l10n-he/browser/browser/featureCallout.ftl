# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = הבא

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = הבנתי!

## Firefox View feature tour strings

callout-firefox-view-recently-closed-subtitle = כל הלשוניות שסגרת יופיעו כאן באורח פלא. לעולם לא יהיה צורך עוד לדאוג לגבי סגירת לשוניות בטעות.

## Continuous Onboarding - Firefox View: Tab pick up

continuous-onboarding-firefox-view-tab-pickup-subtitle = קבלת גישה ללשוניות הפתוחות שלך מכל מכשיר. בנוסף, סנכרון הסימניות, הססמאות שלך ועוד.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = תחילת עבודה
