# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Viŝi

## Placeholders for date and time inputs

datetime-year-placeholder = jjjj
datetime-month-placeholder = mm
datetime-day-placeholder = tt
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Jaro
datetime-month =
    .aria-label = Monato
datetime-day =
    .aria-label = Tago

## Field labels for input type=time

datetime-hour =
    .aria-label = Horoj
datetime-minute =
    .aria-label = Minutoj
datetime-second =
    .aria-label = Sekundoj
datetime-millisecond =
    .aria-label = Milisekundoj
datetime-dayperiod =
    .aria-label = AM/PM
