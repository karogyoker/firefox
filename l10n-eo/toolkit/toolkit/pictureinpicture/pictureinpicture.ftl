# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Bildo en bildo

pictureinpicture-pause =
    .aria-label = Paŭzigi
pictureinpicture-play =
    .aria-label = Ludi

pictureinpicture-mute =
    .aria-label = Silentigi
pictureinpicture-unmute =
    .aria-label = Malsilentigi

pictureinpicture-unpip =
    .aria-label = Sendi reen al langeto

pictureinpicture-close =
    .aria-label = Fermi

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Paŭzigi
    .title = Paŭzigi (spacoklavo)
pictureinpicture-play-cmd =
    .aria-label = Ludi
    .title = Ludi (spacoklavo)

pictureinpicture-mute-cmd =
    .aria-label = Silentigi
    .title = Silentigi ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Malsilentigi
    .title = Malsilentigi ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Sendi reen al langeto
    .title = Reen al langeto

pictureinpicture-close-cmd =
    .aria-label = Fermi
    .title = Fermi ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Subtekstoj
    .title = Subtekstoj

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Plenekrane
    .title = Plenekrane (duobla alklako)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Eliri el plenekrana reĝimo
    .title = Eliri el plenekrana reĝimo (duobla alklako)

pictureinpicture-seekbackward-cmd =
    .aria-label = Malantaŭen
    .title = Malantaŭen (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Antaŭen
    .title = Antaŭen (→)

pictureinpicture-subtitles-label = Subtekstoj

pictureinpicture-font-size-label = Tipara grando

pictureinpicture-font-size-small = Eta

pictureinpicture-font-size-medium = Mezgranda

pictureinpicture-font-size-large = Granda
