# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Antaŭen

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Mi komprenis!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Iru tien kaj reen inter aparatoj per spegulitaj langetoj

callout-firefox-view-tab-pickup-subtitle = Rapide prenu langetojn el via telefono kaj malfermu ilin rekte ĉi tie.

callout-firefox-view-recently-closed-title = Tuj rehavu viajn fermitajn langetojn

callout-firefox-view-recently-closed-subtitle = Ĉiuj viaj fermitaj langetoj aperos magie ĉi tie. Neniam plu zorgu pri nevola fermo de retejo.

callout-firefox-view-colorways-title = Aldonu koloran tuŝeton

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Per koloraro elektu nuancon kiu kongruas kun vi. Nur en { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Esploru niajn lastajn kolorarojn

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Kolorigu viaj retumilon per tiu ĉi nuancoj, inspiritaj de sendependaj voĉoj. Nur en { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Plibonigu vian retumon per spegulitaj langetoj

continuous-onboarding-firefox-view-tab-pickup-subtitle = Aliri viajn malfermitajn langetojn el iu ajn aparato. Cetere spegulu viajn legosignojn, pasvortojn kaj pli.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Unuaj paŝoj
