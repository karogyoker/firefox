# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = ეკრანი-ეკრანში

pictureinpicture-pause =
    .aria-label = შეჩერება
pictureinpicture-play =
    .aria-label = გაშვება

pictureinpicture-mute =
    .aria-label = დადუმება
pictureinpicture-unmute =
    .aria-label = ხმის ჩართვა

pictureinpicture-unpip =
    .aria-label = დაბრუნება ჩანართში

pictureinpicture-close =
    .aria-label = დახურვა

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = შეჩერება
    .title = შეჩერება (ჰარი)
pictureinpicture-play-cmd =
    .aria-label = გაშვება
    .title = გაშვება (ჰარი)

pictureinpicture-mute-cmd =
    .aria-label = დადუმება
    .title = დადუმება ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = ახმოვანება
    .title = ახმოვანება ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = დაბრუნება ჩანართში
    .title = დაბრუნება ჩანართში

pictureinpicture-close-cmd =
    .aria-label = დახურვა
    .title = დახურვა ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = ზედწარწერები
    .title = სუბტიტრები

##

pictureinpicture-fullscreen-cmd =
    .aria-label = სრულ ეკრანზე
    .title = სრულეკრანიანი (ორმაგი წკაპი)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = გამოსვლა სრული ეკრანიდან
    .title = სრულეკრანიანიდან გამოსვლა (ორმაგი წკაპი)

pictureinpicture-seekbackward-cmd =
    .aria-label = უკუსვლა
    .title = უკან გადახვევა (←)

pictureinpicture-seekforward-cmd =
    .aria-label = წინსვლა
    .title = წინ გადახვევა (→)

pictureinpicture-subtitles-label = ზედწარწერა

pictureinpicture-font-size-label = შრიფტის ზომა

pictureinpicture-font-size-small = მცირე

pictureinpicture-font-size-medium = საშუალო

pictureinpicture-font-size-large = დიდი
