# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Nesaf

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Iawn!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Symudwch rhwng dyfeisiau gyda chipio tabiau

callout-firefox-view-tab-pickup-subtitle = Cipiwch dabiau agored yn gyflym o'ch ffôn a'u hagor yma i gael y llif gorau.

callout-firefox-view-recently-closed-title = Ail-agorwch eich tabiau caeedig mewn chwinciad

callout-firefox-view-recently-closed-subtitle = Bydd eich holl dabiau caeedig yn ymddangos yn hudol yma. Peidiwch byth â phoeni am gau gwefan ar ddamwain eto.

callout-firefox-view-colorways-title = Ychwanegwch bach o liw

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Dewiswch yr arlliw sy'n siarad â chi gyda llwybrau lliw. Dim ond yn { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Archwiliwch ein llwybrau lliw diweddaraf

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Lliwiwch eich porwr gyda'r arlliwiau eiconig hyn, wedi'u hysbrydoli gan leisiau annibynnol. Dim ond yn { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Rhowch hwb i'ch pori trwy ailagor tab

continuous-onboarding-firefox-view-tab-pickup-subtitle = Cyrchwch eich tabiau agored o unrhyw ddyfais. Hefyd cydweddwch eich nodau tudalen, cyfrineiriau, a rhagor.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Cychwyn arni
