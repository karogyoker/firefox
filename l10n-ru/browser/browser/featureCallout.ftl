# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Далее

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Понятно!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Переходите между устройствами и забирайте вкладки

callout-firefox-view-tab-pickup-subtitle = Быстро заберите открытые вкладки со своего телефона и открывайте их здесь для продолжения работы.

callout-firefox-view-recently-closed-title = Быстро верните закрытые вкладки

callout-firefox-view-recently-closed-subtitle = Все ваши закрытые вкладки волшебным образом появятся здесь. Больше никогда не беспокойтесь, что случайно закрыли сайт.

callout-firefox-view-colorways-title = Добавьте всплеск цветов

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Выберите цвет, подходящий именно вам, с помощью расцветок. Только в { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Ознакомьтесь с нашими последними расцветками

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Раскрасьте свой браузер этими знаковыми оттенками, вдохновленными независимыми голосами. Только в { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Ускорьте веб-сёрфинг с помощью выбора вкладок

continuous-onboarding-firefox-view-tab-pickup-subtitle = Доступ к открытым вкладкам с любого устройства. Также синхронизируйте свои закладки, пароли и многое другое.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Начать
