# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Ďalej

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Rozumiem

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Preskakujte medzi zariadeniami pomocou funkcie vyzdvihnutia kariet

callout-firefox-view-tab-pickup-subtitle = Rýchlo si preneste otvorené karty z telefónu a otvorte ich tu, aby ste nestrácali čas.

callout-firefox-view-recently-closed-title = Instantne získajte späť svoje zatvorené karty

callout-firefox-view-recently-closed-subtitle = Všetky vaše zatvorené karty sa tu zázračne zobrazia. Už sa nikdy nemusíte báť náhodného zatvorenia stránky.

callout-firefox-view-colorways-title = Pridajte si kvapku svojej farby

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Vyberte si odtieň, ktorý sa vám páči pomocou farebných tém. Iba vo { -brand-product-name(case: "loc") }.

callout-firefox-view-colorways-reminder-title = Preskúmajte naše najnovšie farebné témy

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Vyfarbite svoj prehliadač týmito ikonickými odtieňmi inšpirovanými nezávislými hlasmi. Iba vo { -brand-product-name(case: "loc") }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Zlepšite svoje prehliadanie pomocou funkcie vyzdvihnutia kariet

continuous-onboarding-firefox-view-tab-pickup-subtitle = Pristupujte k otvoreným kartám z akéhokoľvek zariadenia. Navyše synchronizujte svoje záložky, heslá a ďalšie položky.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Začíname
