# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Colorway collections

colorway-collection-independent-voices = Ahots independenteak
colorway-collection-independent-voices-description = Koloreek kultura alda dezakete. Azken kolore-konbinazioek mundua toki hobea egiten duten ahotsak ospatzen dituzte.
# Used as a subheading for the colorway collection card in about:addons
colorway-collection-independent-voices-short-description = Kultura aldatzen duten koloreak.

## Colorway names and descriptions

