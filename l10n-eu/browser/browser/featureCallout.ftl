# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Hurrengoa

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Ulertu dut!

## Firefox View feature tour strings

callout-firefox-view-colorways-title = Gehitu kolore ukitu bat

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Aukeratu kolore-konbinazioekin ongien datorkizun ñabardura. { -brand-product-name }(e)n bakarrik.

## Continuous Onboarding - Firefox View: Tab pick up

