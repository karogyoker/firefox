# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Stizzar

## Placeholders for date and time inputs

datetime-year-placeholder = oooo
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Onn
datetime-month =
    .aria-label = Mais
datetime-day =
    .aria-label = Di

## Field labels for input type=time

datetime-hour =
    .aria-label = Uras
datetime-minute =
    .aria-label = Minutas
datetime-second =
    .aria-label = Secundas
datetime-millisecond =
    .aria-label = Millisecundas
datetime-dayperiod =
    .aria-label = AM/PM
