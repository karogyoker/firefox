# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-pair-device-dialog =
    .title = Під'єднати інший пристрій
    .style = width: 26em; min-height: 35em;

fxa-qrcode-heading-step1 = 1. Якщо ви ще цього не зробили, встановіть <a data-l10n-name="connect-another-device">Firefox на свій мобільний пристрій</a>.

fxa-qrcode-heading-step2 = 2. Відкрийте Firefox на своєму мобільному пристрої.

fxa-qrcode-heading-step3 = 3. Відкрийте <b>меню</b> (<img data-l10n-name="ios-menu-icon"/> або <img data-l10n-name="android-menu-icon"/>), оберіть <img data-l10n-name="settings-icon"/> <b>Налаштування</b> та виберіть <b>Увімкнути синхронізацію</b>

fxa-qrcode-heading-step4 = 4. Скануйте цей код:

fxa-pair-device-dialog-sync =
    .style = width: 32em;

fxa-pair-device-dialog-sync2 =
    .style = min-width: 32em;

fxa-qrcode-pair-title = Синхронізуйте { -brand-product-name } на своєму телефоні чи планшеті
fxa-qrcode-pair-step1 = 1. Відкрийте { -brand-product-name } на своєму мобільному пристрої.

fxa-qrcode-pair-step2 = 2. Відкрийте <strong>меню</strong> (<img data-l10n-name="ios-menu-icon"/> на iOS або <img data-l10n-name="android-menu-icon"/> Android) і торкніться <strong>Увійти до синхронізації</strong>

fxa-qrcode-pair-step3 = 3. Торкніться <strong> Готовий до сканування </strong> та утримуйте телефон навпроти цього коду

fxa-qrcode-error-title = Сполучення пройшло невдало.

fxa-qrcode-error-body = Спробувати знову.
