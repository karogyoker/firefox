# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Далі

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Зрозуміло!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Перемикайтеся між пристроями з доступом до своїх вкладок

callout-firefox-view-tab-pickup-subtitle = Швидко отримуйте відкриті вкладки з телефона та відкривайте їх тут для продовження роботи.

callout-firefox-view-recently-closed-title = Миттєво відновлюйте закриті вкладки

callout-firefox-view-recently-closed-subtitle = Усі ваші закриті вкладки з'являтимуться тут. Більше ніколи не хвилюйтеся про випадкове закриття сторінки.

callout-firefox-view-colorways-title = Додайте сплеск кольорів

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Оберіть відтінок, який пасуватиме саме вам, за допомогою забарвлень. Лише в { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Ознайомтеся з нашими найновішими забарвленнями

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Розфарбуйте свій браузер цими знаковими відтінками, натхненними незалежними голосами. Тільки в { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Пришвидшіть свій перегляд за допомогою синхронізованих вкладок

continuous-onboarding-firefox-view-tab-pickup-subtitle = Отримуйте доступ до ваших відкритих вкладок з будь-якого пристрою. Крім того, синхронізуйте свої закладки, паролі та інші дані.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Почнемо
