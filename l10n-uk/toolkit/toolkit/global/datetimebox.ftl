# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Очистити

## Placeholders for date and time inputs

datetime-year-placeholder = рррр
datetime-month-placeholder = мм
datetime-day-placeholder = дд
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Рік
datetime-month =
    .aria-label = Місяць
datetime-day =
    .aria-label = День

## Field labels for input type=time

datetime-hour =
    .aria-label = Години
datetime-minute =
    .aria-label = Хвилини
datetime-second =
    .aria-label = Секунди
datetime-millisecond =
    .aria-label = Мілісекунди
datetime-dayperiod =
    .aria-label = AM/PM
