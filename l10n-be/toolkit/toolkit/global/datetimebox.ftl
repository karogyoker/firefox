# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Ачысціць

## Placeholders for date and time inputs

datetime-year-placeholder = гггг
datetime-month-placeholder = мм
datetime-day-placeholder = дд
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Год
datetime-month =
    .aria-label = Месяц
datetime-day =
    .aria-label = Дзень

## Field labels for input type=time

datetime-hour =
    .aria-label = Гадзіны
datetime-minute =
    .aria-label = Хвіліны
datetime-second =
    .aria-label = Секунды
datetime-millisecond =
    .aria-label = Мілісекунды
datetime-dayperiod =
    .aria-label = AM/PM
