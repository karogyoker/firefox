# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Выява ў выяве

pictureinpicture-pause =
    .aria-label = Прыпыніць
pictureinpicture-play =
    .aria-label = Граць

pictureinpicture-mute =
    .aria-label = Заглушыць
pictureinpicture-unmute =
    .aria-label = Уключыць гук

pictureinpicture-unpip =
    .aria-label = Адправіць назад на картку

pictureinpicture-close =
    .aria-label = Закрыць

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Прыпыніць
    .title = Прыпыніць (Прабел)
pictureinpicture-play-cmd =
    .aria-label = Граць
    .title = Прайграваць (Прабел)

pictureinpicture-mute-cmd =
    .aria-label = Заглушыць
    .title = Адключыць гук ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Уключыць гук
    .title = Уключыць гук ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Адправіць назад на картку
    .title = Назад на картку

pictureinpicture-close-cmd =
    .aria-label = Закрыць
    .title = Закрыць ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Субцітры
    .title = Субцітры

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Увесь экран
    .title = Поўнаэкранны рэжым (падвойны пстрык)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Выйсці з поўнага экрана
    .title = Выйсці з поўнаэкраннага рэжыму (падвойны пстрык)

pictureinpicture-seekbackward-cmd =
    .aria-label = Назад
    .title = Назад (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Наперад
    .title = Наперад (→)

pictureinpicture-subtitles-label = Субцітры

pictureinpicture-font-size-label = Памер шрыфту

pictureinpicture-font-size-small = Малы

pictureinpicture-font-size-medium = Сярэдні

pictureinpicture-font-size-large = Вялікі
