# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Weiter

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Verstanden!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Wechseln Sie zwischen den Geräten mit synchronisierten Tabs

callout-firefox-view-tab-pickup-subtitle = Greifen Sie schnell auf offene Tabs von Ihrem Telefon zu, und öffnen Sie diese hier für unterbrechungsfreies Surfen.

callout-firefox-view-recently-closed-title = Holen Sie sich geschlossene Tabs im Handumdrehen zurück

callout-firefox-view-recently-closed-subtitle = Alle Ihre geschlossenen Tabs werden hier wie von Zauberhand angezeigt. Machen Sie sich nie wieder Sorgen, eine Website versehentlich zu schließen.

callout-firefox-view-colorways-title = Fügen Sie einen Farbtupfer hinzu

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Wählen Sie mit Farbwelten den Farbton, der Sie anspricht. Nur in { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Entdecken Sie unsere neuesten Farbwelten

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Färben Sie Ihren Browser mit diesen ikonischen Farbtönen, inspiriert von unabhängigen Stimmen. Nur in { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Verbessern Sie Ihr Surferlebnis mit synchronisierten Tabs

continuous-onboarding-firefox-view-tab-pickup-subtitle = Greifen Sie von jedem Gerät aus auf Ihre geöffneten Tabs zu. Synchronisieren Sie zusätzlich Ihre Lesezeichen, Passwörter und mehr.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Erste Schritte
