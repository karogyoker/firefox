# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = ถัดไป

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = เข้าใจแล้ว!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = สับเปลี่ยนไปมาระหว่างอุปกรณ์ต่าง ๆ ด้วยการรับแท็บ

callout-firefox-view-tab-pickup-subtitle = นำแท็บที่เปิดอยู่จากโทรศัพท์ของคุณมาเปิดที่นี่อย่างรวดเร็วเพื่อให้คุณใช้งานได้อย่างลื่นไหล

callout-firefox-view-recently-closed-title = นำแท็บที่ปิดไปแล้วของคุณกลับคืนมาในพริบตา

callout-firefox-view-recently-closed-subtitle = แท็บทั้งหมดที่ปิดไปแล้วของคุณจะปรากฏขึ้นที่นี่ เพื่อไม่ให้คุณต้องกังวลกับการปิดไซต์โดยไม่ได้ตั้งใจอีกต่อไป

callout-firefox-view-colorways-title = เลือกสีสันที่คุณชอบ

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = เลือกเฉดสีที่บ่งบอกความเป็นตัวคุณ มีให้ใช้เฉพาะใน { -brand-product-name } เท่านั้น

## Continuous Onboarding - Firefox View: Tab pick up

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = เริ่มต้นใช้งาน
