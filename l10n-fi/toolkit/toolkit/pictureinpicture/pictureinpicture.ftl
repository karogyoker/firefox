# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Kuva kuvassa

pictureinpicture-pause =
    .aria-label = Pysäytä
pictureinpicture-play =
    .aria-label = Toista

pictureinpicture-mute =
    .aria-label = Vaimenna ääni
pictureinpicture-unmute =
    .aria-label = Palauta ääni

pictureinpicture-unpip =
    .aria-label = Lähetä takaisin välilehteen

pictureinpicture-close =
    .aria-label = Sulje

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pysäytä
    .title = Pysäytä (välilyönti)
pictureinpicture-play-cmd =
    .aria-label = Toista
    .title = Toista (välilyönti)

pictureinpicture-mute-cmd =
    .aria-label = Vaimenna ääni
    .title = Vaimenna ääni ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Palauta ääni
    .title = Palauta ääni ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Lähetä takaisin välilehteen
    .title = Takaisin välilehteen

pictureinpicture-close-cmd =
    .aria-label = Sulje
    .title = Sulje ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Tekstitykset
    .title = Tekstitykset

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Koko näyttö
    .title = Koko näyttö (kaksoisnapsautus)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Poistu koko näytöstä
    .title = Poistu koko näytön tilasta (kaksoisnapsautus)

pictureinpicture-seekbackward-cmd =
    .aria-label = Taaksepäin
    .title = Taaksepäin (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Eteenpäin
    .title = Eteenpäin (→)

pictureinpicture-subtitles-label = Tekstitykset

pictureinpicture-font-size-label = Kirjasinkoko

pictureinpicture-font-size-small = Pieni

pictureinpicture-font-size-medium = Keskikokoinen

pictureinpicture-font-size-large = Suuri
