# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Келесі
# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Түсіндім!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Құрылғылар арасында ауысып, беттерді алып жүріңіз
callout-firefox-view-tab-pickup-subtitle = Телефоныңыздан ашық беттерді жылдам алып, оларды осы жерде жұмысты жалғастыру үшін ашыңыз.
callout-firefox-view-recently-closed-title = Жабылған беттерді жылдам қайтарыңыз
callout-firefox-view-recently-closed-subtitle = Барлық жабық беттеріңіз осы жерде сиқырлы түрде көрсетіледі. Ешқашан сайтты кездейсоқ жабу туралы енді алаңдамаңыз.
callout-firefox-view-colorways-title = Түстер шашырауын қосыңыз

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Беттерді таңдау арқылы веб-шолуды жылдамдатыңыз
continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Бастау
