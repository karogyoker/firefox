# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Næste

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Forstået!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Hop mellem enheder med synkroniserede faneblade

callout-firefox-view-tab-pickup-subtitle = Hent hurtigt åbne faneblade fra din telefon og åbn dem her.

callout-firefox-view-recently-closed-title = Få hurtigt dine lukkede faneblade tilbage

callout-firefox-view-recently-closed-subtitle = Alle dine lukkede faneblade vil blive vist her, så du behøver aldrig at være bange for at komme til at lukke et websted ved et uheld.

callout-firefox-view-colorways-title = Tilføj lidt farve

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Vælg din personlige nuance med farvekombinationer. Kun i { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Udforsk vores nyeste farvekombinationer

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Sæt farve på din browser med disse ikoniske nuancer, der er inspireret af uafhængige stemmer. Kun i { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Få en bedre browsing-oplevelse med synkroniserede faneblade

continuous-onboarding-firefox-view-tab-pickup-subtitle = Hav adgang til åbne faneblade på alle dine enheder. Og synkroniser dine bogmærker og adgangskoder med mere.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Kom i gang
