# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Selanjutnya

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Paham!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Lompat antar perangkat dengan pengambilan tab

callout-firefox-view-tab-pickup-subtitle = Ambil tab terbuka dari ponsel Anda dengan cepat dan buka di sini untuk alur kerja maksimal.

callout-firefox-view-recently-closed-title = Dapatkan kembali tab tertutup Anda dalam sekejap

callout-firefox-view-recently-closed-subtitle = Semua tab tertutup Anda akan secara ajaib muncul di sini. Tak perlu khawatir lagi akan menutup situs secara tidak sengaja.

callout-firefox-view-colorways-title = Tambahkan percikan warna

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Pilih warna yang berbicara dengan Anda dengan ragam warna. Hanya di { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Jelajahi ragam warna terbaru kami

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Warnai peramban Anda dengan nuansa ikonik ini, terinspirasi oleh suara independen. Hanya di { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Tingkatkan penjelajahan Anda dengan fitur pengambilan tab

continuous-onboarding-firefox-view-tab-pickup-subtitle = Akses tab terbuka Anda dari perangkat mana pun. Dan juga, sinkronkan markah, sandi, dan lainnya.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Memulai
