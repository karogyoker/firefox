# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-picture

pictureinpicture-pause =
    .aria-label = Mombyta
pictureinpicture-play =
    .aria-label = Mboheta

pictureinpicture-mute =
    .aria-label = Mokirirĩ
pictureinpicture-unmute =
    .aria-label = Mba’epu mbojevy

pictureinpicture-unpip =
    .aria-label = Emondojey tendayképe

pictureinpicture-close =
    .aria-label = Mboty

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Mombyta
    .title = Mombyta (momombyryha)
pictureinpicture-play-cmd =
    .aria-label = Mbopu
    .title = Mbopu (momombyryha)

pictureinpicture-mute-cmd =
    .aria-label = Mokirirĩ
    .title = Mokirirĩ ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Embopu
    .title = Embopu ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Emondojey tendayképe
    .title = Ejevyjey tendayképe

pictureinpicture-close-cmd =
    .aria-label = Mboty
    .title = Mboty ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Teratee’i
    .title = Teratee’i

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Mba’erechaha tuichakue
    .title = Emboja isaja’ỹre (mokõi jekutu)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Esẽ mba’erechaha tuichavévagui
    .title = Esẽ mba’erechaha tuichavévagui (mokõi jekutu)

pictureinpicture-seekbackward-cmd =
    .aria-label = Tapykue
    .title = Tapykue (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Tenonde
    .title = Tenonde (→)

pictureinpicture-subtitles-label = Teratee’i

pictureinpicture-font-size-label = Tai tuichakue

pictureinpicture-font-size-small = Michĩva

pictureinpicture-font-size-medium = Mbyteguáva

pictureinpicture-font-size-large = Tuicháva
