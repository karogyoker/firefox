# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Mopotĩ

## Placeholders for date and time inputs

datetime-year-placeholder = yyyy
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Ary
datetime-month =
    .aria-label = Jasy
datetime-day =
    .aria-label = Ára

## Field labels for input type=time

datetime-hour =
    .aria-label = Aravo
datetime-minute =
    .aria-label = Aravo’i
datetime-second =
    .aria-label = Aravo’ive
datetime-millisecond =
    .aria-label = Aravo’ieteve
datetime-dayperiod =
    .aria-label = AM/PM
