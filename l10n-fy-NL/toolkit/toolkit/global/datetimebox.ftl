# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Wiskje

## Placeholders for date and time inputs

datetime-year-placeholder = jjjj
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Jier
datetime-month =
    .aria-label = Moanne
datetime-day =
    .aria-label = Dei

## Field labels for input type=time

datetime-hour =
    .aria-label = Oeren
datetime-minute =
    .aria-label = Minuten
datetime-second =
    .aria-label = Sekonden
datetime-millisecond =
    .aria-label = Millisekonden
datetime-dayperiod =
    .aria-label = AM/PM
