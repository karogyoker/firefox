# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Folgjende

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Begrepen!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Ljep tusken apparaten mei opheljen fan ljepblêden

callout-firefox-view-tab-pickup-subtitle = Iepenje hjir fluch ljepblêden fan jo telefoan en gean fierder wêr’t jo bleaun wiene.

callout-firefox-view-recently-closed-title = Iepenje ienfâldichwei jo krekt sluten ljepblêden

callout-firefox-view-recently-closed-subtitle = Al jo sletten ljepblêden sille hjir op magyske wize ferskine. Nea wer soargen oer it mei fersin sluten fan in website.

callout-firefox-view-colorways-title = Foegje wat kleur ta

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Kies it kleur dy’t jo oansprekt mei kleurstellingen. Allinnich yn { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Untdek ús nijste kleurstellingen

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Kleur jo browser mei dizze ikoanyske tinten, ynspirearre troch ûnôfhinklike stimmen. Allinnich yn { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Jou jo navigaasje in boost mei it opheljen fan ljepblêden

continuous-onboarding-firefox-view-tab-pickup-subtitle = Tagong ta jo iepene ljepblêden fan elk apparaat ôf. En syngronisearje jo blêdwizers, wachtwurden en mear.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Begjinne
