# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-button-firefox-view =
    .label = { -firefoxview-brand-name }
    .tooltiptext = { -firefoxview-brand-name }

menu-tools-firefox-view =
    .label = { -firefoxview-brand-name }
    .accesskey = F

firefoxview-page-title = { -firefoxview-brand-name }

firefoxview-close-button =
    .title = Slute
    .aria-label = Slute

# Used instead of the localized relative time when a timestamp is within a minute or so of now
firefoxview-just-now-timestamp = Sa krekt

# This is a headline for an area in the product where users can resume and re-open tabs they have previously viewed on other devices.
firefoxview-tabpickup-header = Syngronisearre ljepblêden
firefoxview-tabpickup-description = Siden fan oare apparaten ôf iepenje

firefoxview-tabpickup-recenttabs-description = Hjir komt de list mei resinte ljepblêden

# Variables:
#  $percentValue (Number): the percentage value for setup completion
firefoxview-tabpickup-progress-label = { $percentValue }% foltôge

firefoxview-tabpickup-step-signin-header = Wikselje maklik tusken apparaten
firefoxview-tabpickup-step-signin-description = Meld jo earst oan of meitsje in account om hjir de ljepblêden fan jo telefoan te iepenjen.
firefoxview-tabpickup-step-signin-primarybutton = Trochgean

firefoxview-tabpickup-adddevice-header = { -brand-product-name } op jo telefoan of tablet syngronisearje
firefoxview-tabpickup-adddevice-description = Download { -brand-product-name } foar mobyl en meld jo dêr oan.
firefoxview-tabpickup-adddevice-learn-how = Mear ynfo
firefoxview-tabpickup-adddevice-primarybutton = { -brand-product-name } foar mobyl downloade

firefoxview-tabpickup-synctabs-header = Ljepblêdsyngronisaasje ynskeakelje
firefoxview-tabpickup-synctabs-description = { -brand-short-name } tastean om ljepblêden tusken apparaten te dielen.
firefoxview-tabpickup-synctabs-learn-how = Mear ynfo
firefoxview-tabpickup-synctabs-primarybutton = Iepen ljepblêden syngronisearje

firefoxview-tabpickup-fxa-admin-disabled-header = Jo organisaasje hat syngronisaasje útskeakele
firefoxview-tabpickup-fxa-admin-disabled-description = { -brand-short-name } is net yn steat om ljepblêden tusken apparaten te syngronisearjen, omdat jo behearder syngronisaasje útskeakele hat.

firefoxview-tabpickup-network-offline-header = Kontrolearje jo ynternetferbining
firefoxview-tabpickup-network-offline-description = As jo in firewall of proxy brûke, kontrolearje dan oft { -brand-short-name } tastimming hat om tagong te krijen ta it web.
firefoxview-tabpickup-network-offline-primarybutton = Opnij probearje

firefoxview-tabpickup-sync-error-header = Wy hawwe problemen mei syngronisaasje
firefoxview-tabpickup-generic-sync-error-description = { -brand-short-name } kin de syngronisaasjetsjinst no net berikke. Probearje it oer in pear mominten nochris.
firefoxview-tabpickup-sync-error-primarybutton = Opnij probearje

firefoxview-tabpickup-sync-disconnected-header = Skeakelje syngronisaasje yn om troch te gean
firefoxview-tabpickup-sync-disconnected-description = Om jo ljepblêden op te pakken, moatte jo syngronisaasje yn { -brand-short-name } tastean.
firefoxview-tabpickup-sync-disconnected-primarybutton = Syngronisaasje ynskeakelje yn ynstellingen

firefoxview-tabpickup-syncing = Bliuw sitten wylst jo ljepblêden syngronisearje. It duorret mar in amerijke.

firefoxview-mobile-promo-header = Iepenje ljepblêden fan jo telefoan of tablet
firefoxview-mobile-promo-description = Meld jo oan by { -brand-product-name } op iOS of Android om jo lêste mobile ljepblêden te besjen.
firefoxview-mobile-promo-primarybutton = { -brand-product-name } foar mobyl downloaden

firefoxview-mobile-confirmation-header = 🎉 Jo binne ree!
firefoxview-mobile-confirmation-description = No kinne jo jo { -brand-product-name }-ljepblêden fan jo tablet of telefoan iepenje.

firefoxview-closed-tabs-title = Koartlyn sluten

firefoxview-closed-tabs-description = Siden dy't jo sluten hawwe op dit apparaat opnij iepenje.

firefoxview-closed-tabs-description2 = Iepenje siden opnij dy’t jo yn dit finster sluten hawwe.
firefoxview-closed-tabs-placeholder = <strong>Gjin koartlyn sletten siden</strong><br/>Wês nea bang wer in sletten ljepblêd te ferliezen. Jo kinne it hjir altyd ophelje.

# refers to the last tab that was used
firefoxview-pickup-tabs-badge = Lêst aktyf

# Variables:
#   $targetURI (string) - URL that will be opened in the new tab
firefoxview-tabs-list-tab-button =
    .title = { $targetURI } iepenje yn in nij ljepblêd

firefoxview-try-colorways-button = Kleurstellingen probearje
firefoxview-no-current-colorway-collection = Nije kleurstellingen binne ûnderweis
firefoxview-change-colorway-button = Kleurstelling wizigje

# Variables:
#  $intensity (String): Colorway intensity
#  $collection (String): Colorway Collection name
firefoxview-colorway-description = { $intensity } · { $collection }

firefoxview-synced-tabs-placeholder = <strong>Noch neat te sjen</strong><br/>De folgjende kear as jo in side iepenje yn { -brand-product-name } op in oar apparaat, iepenje it hjir as yn magy.

firefoxview-collapse-button-show =
    .title = List toane

firefoxview-collapse-button-hide =
    .title = List ferstopje
