# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = ਅੱਗੇ

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = ਇਹ ਲਵੋ!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = ਚੁਣੀ ਟੈਬ ਨਾਲ ਡਿਵਾਈਸਾਂ ਵਿਚਾਲੇ ਜਾਓ

callout-firefox-view-tab-pickup-subtitle = ਆਪਣੇ ਫ਼ੋਨ ਤੋਂ ਖੋਲ੍ਹੀਆਂ ਟੈਬਾਂ ਫ਼ੌਰਨ ਫੜੋ ਅਤੇ ਇੱਥੇ ਉਹਨਾਂ ਨੂੰ ਵੱਧ ਤੋਂ ਵੱਧ ਤੇਜ਼ੀ ਨਾਲ ਖੋਲ੍ਹੋ।

callout-firefox-view-recently-closed-title = ਆਪਣੀਆਂ ਬੰਦ ਕੀਤੀਆਂ ਟੈਬਾਂ ਨੂੰ ਚੁਕਟੀ ਵਿੱਚ ਵਾਪਸ ਲਵੋ

callout-firefox-view-recently-closed-subtitle = ਤੁਹਾਡੀਆਂ ਸਾਰੀਆਂ ਬੰਦ ਕੀਤੀਆਂ ਟੈਬਾਂ ਛੂ ਮੰਤਰ ਨਾਲ ਇੱਥੇ ਆ ਜਾਣਗੀਆਂ। ਮੁੜ ਕੇ ਕਦੇ ਵੀ ਅਚਾਨਕ ਬੰਦ ਹੋਈ ਸਾਈਟ ਬਾਰੇ ਪਰਵਾਹ ਨਾ ਕਰੋ।

callout-firefox-view-colorways-title = ਰੰਗ ਛਿੜਕੋ

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = ਉਹ ਰੰਗ ਚੁਣੋ, ਜੋ ਤੁਹਾਡੇ ਰੰਗ-ਢੰਗ ਮੁਤਾਬਕ ਹੋਵੇ। ਸਿਰਫ { -brand-product-name } ਵਿੱਚ ਹੀ।

callout-firefox-view-colorways-reminder-title = ਸਾਡੇ ਨਵੇਂ ਰੰਗ-ਢੰਗ ਦੀ ਪੜਤਾਲ ਕਰੋ

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = ਇਹ ਨਿਸ਼ਾਨ ਵਾਲੇ ਸ਼ੇਡਾਂ ਨਾਲ ਆਪਣੇ ਬਰਾਊਜ਼ਰ ਨੂੰ ਰੰਗੋ, ਜੋ ਕਿ ਆਜ਼ਾਦ ਆਵਾਜ਼ਾਂ ਤੋਂ ਪ੍ਰੇਰਿਤ ਹਨ। ਸਿਰਫ਼ { -brand-product-name } ਨਾਲ।

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = ਟੈਬ ਚੋਣ ਨਾਲ ਆਪਣੇ ਬਰਾਊਜ਼ ਕਰਨ ਵਿੱਚ ਵਾਧਾ ਕਰੋ

continuous-onboarding-firefox-view-tab-pickup-subtitle = ਕਿਸੇ ਵੀ ਡਿਵਾਈਸ ਤੋਂ ਆਪਣੀਆਂ ਟੈਬਾਂ ਨੂੰ ਵੇਖੋ, ਨਾਲ ਹੀ ਆਪਣੇ ਬੁੱਕਮਾਰਕਾਂ, ਪਾਸਵਰਡਾਂ ਤੇ ਹੋਰ ਨੂੰ ਸਿੰਕ ਵੀ ਕਰੋ।

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = ਸ਼ੁਰੂ ਕਰੀਏ
