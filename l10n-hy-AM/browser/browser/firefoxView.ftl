# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

firefoxview-close-button =
    .title = Փակել
    .aria-label = Փակել

# Used instead of the localized relative time when a timestamp is within a minute or so of now
firefoxview-just-now-timestamp = Հենց հիմա

# This is a headline for an area in the product where users can resume and re-open tabs they have previously viewed on other devices.
firefoxview-tabpickup-header = Ներդիրի հավաքում

firefoxview-tabpickup-adddevice-learn-how = Իմանալ ինչպես

firefoxview-tabpickup-sync-error-primarybutton = Կրկին փորձել

