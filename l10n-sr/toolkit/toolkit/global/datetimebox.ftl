# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Очисти

## Placeholders for date and time inputs

datetime-year-placeholder = yyyy
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Година
datetime-month =
    .aria-label = Месец
datetime-day =
    .aria-label = Дан

## Field labels for input type=time

datetime-hour =
    .aria-label = Сати
datetime-minute =
    .aria-label = Минути
datetime-second =
    .aria-label = Секунде
datetime-millisecond =
    .aria-label = Милисекунде
datetime-dayperiod =
    .aria-label = AM/PM
