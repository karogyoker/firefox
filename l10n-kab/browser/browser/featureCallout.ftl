# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Uḍfir

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Gziɣ!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Ɛeddi seg yibenk ɣer wayeḍ s ulqaḍ n waccaren

callout-firefox-view-tab-pickup-subtitle = Kkes-d s zzerb accaren yeldin seg tiliɣri-inek·inem, teldiḍ-ten dagi i uktum uffay.

callout-firefox-view-recently-closed-title = Err-d accaren-ik·im yettwamedlen s zzerb

callout-firefox-view-recently-closed-subtitle = Akk accaren-ik·im i imedlen ad d-banen dagi war ma tfaqqeḍ. Ɣas ur ttagad ma yella yettwamdel kra n usmel weḥd-s.

callout-firefox-view-colorways-title = Rnu ṛucc s yini

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Fren tiɣmi ara ak·am-d-yettmeslayen s ufran n yiniten. Yella kan deg { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Snirem afran-nneɣ n yiniten aneggaru

## Continuous Onboarding - Firefox View: Tab pick up

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Bdu
