# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Επόμενο

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Εντάξει!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Εναλλαγή συσκευών με μεταφορά καρτελών

callout-firefox-view-tab-pickup-subtitle = Λάβετε γρήγορα τις ανοικτές καρτέλες από το τηλέφωνό σας και ανοίξτε τις εδώ για απρόσκοπτη εργασία.

callout-firefox-view-recently-closed-title = Άμεση επιστροφή σε κλειστές καρτέλες

callout-firefox-view-recently-closed-subtitle = Όλες οι κλειστές καρτέλες σας θα εμφανιστούν εδώ ως δια μαγείας. Έτσι, δεν θα ανησυχείτε ποτέ μήπως κλείσετε ξανά έναν ιστότοπο κατά λάθος.

callout-firefox-view-colorways-title = Προσθέστε μια πινελιά χρώματος

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Επιλέξτε την απόχρωση του χρωματικού συνδυασμού που σας ταιριάζει. Μόνο στο { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Εξερευνήστε τους νεότερους χρωματικούς συνδυασμούς μας

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Χρωματίστε το πρόγραμμα περιήγησής σας με αυτές τις εμβληματικές αποχρώσεις, εμπνευσμένες από ανεξάρτητες φωνές. Μόνο στο { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Ενισχύστε την περιήγησή σας με τη μεταφορά καρτελών

continuous-onboarding-firefox-view-tab-pickup-subtitle = Αποκτήστε πρόσβαση στις ανοικτές καρτέλες σας από οποιαδήποτε συσκευή. Συγχρονίστε τους σελιδοδείκτες σας, τους κωδικούς πρόσβασης και πολλά άλλα.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Έναρξη
