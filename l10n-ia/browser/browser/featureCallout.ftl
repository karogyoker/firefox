# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Sequente

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Comprendite!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Salta ab un apparato al altere con le prisa de scheda

callout-firefox-view-tab-pickup-subtitle = Rapidemente collige schedas aperte de tu telephono e aperi los ci pro maxime fluxo.

callout-firefox-view-recently-closed-title = Re-obtene tu schedas claudite in un snap

callout-firefox-view-recently-closed-subtitle = Tote tu schedas claudite apparera magicamente ci. Jammais preoccupar te ancora del clausura accidental de un sito.

callout-firefox-view-colorways-title = Adde un tocco de color

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Elige le umbra que parla pro te con combinationes de colores. Solo in { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Discoperi nostre ultime combinationes de colores

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Colora tu navigator con iste tonalitates emblematic, inspirate per voces independente. Solo in { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Stimula tu navigation con le prisa de schedas

continuous-onboarding-firefox-view-tab-pickup-subtitle = Accede a tu schedas aperte a partir de non importa qual apparato. In ultra, synchronisa tu marcapaginas, contrasignos, e plus.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Comenciar
