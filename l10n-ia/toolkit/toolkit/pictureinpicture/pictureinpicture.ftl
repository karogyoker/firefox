# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Imagine-in-imagine

pictureinpicture-pause =
    .aria-label = Pausar
pictureinpicture-play =
    .aria-label = Reproducer

pictureinpicture-mute =
    .aria-label = Silentiar
pictureinpicture-unmute =
    .aria-label = Non plus silentiar

pictureinpicture-unpip =
    .aria-label = Inviar retro al scheda

pictureinpicture-close =
    .aria-label = Clauder

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pausar
    .title = Pausar (Barra de spatio)
pictureinpicture-play-cmd =
    .aria-label = Reproducer
    .title = Reproducer (Barra de spatio)

pictureinpicture-mute-cmd =
    .aria-label = Silentiar
    .title = Silentiar ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Reactivar audio
    .title = Reactivar audio ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Inviar retro al scheda
    .title = Retro al scheda

pictureinpicture-close-cmd =
    .aria-label = Clauder
    .title = Clauder ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Subtitulos
    .title = Subtitulos

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Plen schermo
    .title = Plen schermo (duple-clic)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Exir del plen schermo
    .title = Exir del plen schermo (duple-clic)

pictureinpicture-seekbackward-cmd =
    .aria-label = Retro
    .title = Retro (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Avante
    .title = Avante (→)

pictureinpicture-subtitles-label = Subtitulos

pictureinpicture-font-size-label = Dimension del litteras

pictureinpicture-font-size-small = Micre

pictureinpicture-font-size-medium = Medie

pictureinpicture-font-size-large = Grande
