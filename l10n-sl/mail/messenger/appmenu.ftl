# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## New Account

appmenu-new-account-panel-title =
    .title = Nov račun

appmenu-new-account-panel =
    .label = Nov račun
    .accesskey = N

appmenu-create-new-mail-account =
    .label = Pridobite nov naslov
    .accesskey = b

appmenu-new-mail-account =
    .label = Obstoječ e-poštni naslov
    .accesskey = O

appmenu-new-calendar =
    .label = Koledar
    .accesskey = K

appmenu-new-chat-account =
    .label = Klepet
    .accesskey = l

appmenu-new-feed =
    .label = Vir
    .accesskey = V

appmenu-new-newsgroup =
    .label = Novičarska skupina
    .accesskey = N

## New Account / Address Book

appmenu-newab-panel-title =
    .title = Nov imenik

appmenu-newab-panel =
    .label = Nov imenik
    .accesskey = i

appmenu-new-addressbook =
    .label = Krajevni imenik
    .accesskey = K

appmenu-new-carddav =
    .label = Imenik CardDav
    .accesskey = C

appmenu-new-ldap =
    .label = Imenik LDAP
    .accesskey = L

## Create

appmenu-create-panel-title =
    .title = Ustvari

appmenu-create-panel =
    .label = Ustvari
    .accesskey = U

appmenu-create-message =
    .label = Sporočilo
    .accesskey = S

appmenu-create-event =
    .label = Dogodek
    .accesskey = g

appmenu-create-task =
    .label = Opravilo
    .accesskey = O

appmenu-create-contact =
    .label = Stik
    .accesskey = k

## Open

appmenu-open-panel =
    .label = Odpri
    .accesskey = O

appmenu-open-panel-title =
    .title = Odpri

appmenu-open-file-panel =
    .label = Odpri iz datoteke
    .accesskey = O

appmenu-open-file-panel-title =
    .title = Odpri iz datoteke

appmenu-open-message =
    .label = Sporočilo …
    .accesskey = S

appmenu-open-calendar =
    .label = Koledar …
    .accesskey = K

## View / Layout

appmenu-view-panel-title =
    .title = Pogled

appmenu-view-panel =
    .label = Pogled
    .accesskey = P

appmenu-font-size-value = Velikost pisave

appmenu-mail-uidensity-value = Gostota

appmenu-uidensity-compact =
    .tooltiptext = Strnjeno

appmenu-uidensity-default =
    .tooltiptext = Privzeto

appmenu-uidensity-relaxed =
    .tooltiptext = Razmaknjeno

appmenuitem-font-size-enlarge =
    .tooltiptext = Povečaj velikost pisave

appmenuitem-font-size-reduce =
    .tooltiptext = Zmanjšajte velikost pisave

# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size } pik
    .tooltiptext = Ponastavi velikost pisave

## Tools

appmenu-tools-panel-title =
    .title = Orodja

appmenu-tools-panel =
    .label = Orodja
    .accesskey = O

appmenu-tools-import =
    .label = Uvozi
    .accesskey = U

appmenu-tools-export =
    .label = Izvozi
    .accesskey = I

appmenu-tools-message-search =
    .label = Išči po sporočilih
    .accesskey = š

appmenu-tools-message-filters =
    .label = Filtri sporočil
    .accesskey = F

appmenu-tools-download-manager =
    .label = Upravitelj prenosov
    .accesskey = p

appmenu-tools-activity-manager =
    .label = Upravitelj dejavnosti
    .accesskey = d

appmenu-tools-dev-tools =
    .label = Orodja za razvijalce
    .accesskey = z

## Help

appmenu-help-panel-title =
    .title = Pomoč

appmenu-help-get-help =
    .label = Poiščite pomoč
    .accesskey = P

appmenu-help-explore-features =
    .label = Raziščite možnosti
    .accesskey = š

appmenu-help-shortcuts =
    .label = Bližnjice na tipkovnici
    .accesskey = ž

appmenu-help-get-involved =
    .label = Pridružite se
    .accesskey = u

appmenu-help-donation =
    .label = Donirajte
    .accesskey = D

appmenu-help-share-feedback =
    .label = Sporočite ideje in povratne informacije
    .accesskey = S

appmenu-help-enter-troubleshoot-mode2 =
    .label = Način za odpravljanje težav …
    .accesskey = r

appmenu-help-exit-troubleshoot-mode2 =
    .label = Izključi način za odpravljanje težav
    .accesskey = I

appmenu-help-troubleshooting-info =
    .label = Podatki za odpravljanje težav
    .accesskey = a

appmenu-help-about-product =
    .label = O { -brand-short-name(sklon: "mestnik") }
    .accesskey = O
