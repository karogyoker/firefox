<!--
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-->

<!-- Note to localizers, don't localize the strings 'width' or 'height' -->
<!ENTITY  pageInfoWindow.dimensions  "width: 100ch; height: 38em;">

<!ENTITY  copy.key               "C">
<!ENTITY  copy.label             "Kopiraj">
<!ENTITY  copy.accesskey         "K">
<!ENTITY  selectall.key          "A">
<!ENTITY  selectall.label        "Izberi vse">
<!ENTITY  openHelpMac.key        "?">
<!ENTITY  closeWindow.key        "w">
<!ENTITY  copyLinks.label           "Kopiraj povezavo">
<!ENTITY  openInNewTab.label        "Odpri v novem zavihku">
<!ENTITY  openInNewWindow.label     "Odpri v novem oknu">

<!ENTITY  generalTab            "Splošno">
<!ENTITY  generalTitle          "Naslov:">
<!ENTITY  generalURL            "Spletni naslov:">
<!ENTITY  generalType           "Vrsta:">
<!ENTITY  generalMode           "Način izrisovanja:">
<!ENTITY  generalSize           "Velikost:">
<!ENTITY  generalSource         "Vir predpomnilnika:">
<!ENTITY  generalModified       "Spremenjeno:">
<!ENTITY  generalEncoding2      "Kodiranje besedila:">
<!ENTITY  generalMetaName       "Ime">
<!ENTITY  generalMetaContent    "Vsebina">
<!ENTITY  generalSecurityDetails           "Podrobnosti">
<!ENTITY  generalSecurityDetails.accesskey "d">

<!ENTITY  formsTab              "Obrazci">
<!ENTITY  formMethod            "Metoda">
<!ENTITY  formName              "Ime">
<!ENTITY  formEncoding          "Kodiranje:">
<!ENTITY  formTarget            "Cilj:">
<!ENTITY  formFields            "Polja:">
<!ENTITY  formLabel             "Oznaka">
<!ENTITY  formFName             "Ime polja">
<!ENTITY  formType              "Vrsta">
<!ENTITY  formCValue            "Trenutna vrednost">

<!ENTITY  linksTab              "Povezave">
<!ENTITY  linkName              "Ime">
<!ENTITY  linkAddress           "Naslov">
<!ENTITY  linkType              "Vrsta">
<!ENTITY  linkTarget            "Cilj">
<!ENTITY  linkAccessKey         "Ključ za dostop">

<!ENTITY  mediaTab              "Večpredstavnost">
<!ENTITY  mediaTab.accesskey    "V">
<!ENTITY  mediaLocation         "Naslov:">
<!ENTITY  mediaText             "Povezano besedilo:">
<!ENTITY  mediaAltHeader        "Nadomestno besedilo">
<!ENTITY  mediaAddress          "Naslov">
<!ENTITY  mediaType             "Vrsta">
<!ENTITY  mediaSize             "Velikost">
<!ENTITY  mediaCount            "Število">
<!ENTITY  mediaDimension        "Mere:">
<!ENTITY  mediaLongdesc         "Dolg opis:">
<!ENTITY  mediaSaveAs           "Shrani kot …">
<!ENTITY  mediaSaveAs.accesskey "k">
<!ENTITY  mediaPreview          "Predogled:">

<!ENTITY  feedTab               "Viri">
<!ENTITY  feedSubscribe         "Naroči se">
<!ENTITY  feedSubscribe.accesskey "č">

<!ENTITY  permTab               "Dovoljenja">
<!ENTITY  permTab.accesskey     "D">
<!ENTITY  permissionsFor        "Dovoljenja za:">

<!ENTITY  securityTab           "Varnost">
<!ENTITY  securityTab.accesskey "a">
<!ENTITY  securityHeader        "Varnostni podatki za to stran">
<!ENTITY  securityView.certView "Preglej digitalno potrdilo">
<!ENTITY  securityView.accesskey "P">
<!ENTITY  securityView.unknown   "Neznano">


<!ENTITY  securityView.identity.header   "Identiteta spletnega mesta">
<!ENTITY  securityView.identity.owner    "Lastnik:">
<!ENTITY  securityView.identity.domain   "Spletno mesto:">
<!ENTITY  securityView.identity.verifier "Overil:">
<!ENTITY  securityView.identity.validity "Poteče:">

<!ENTITY  securityView.privacy.header                   "Zasebnost in zgodovina">
<!ENTITY  securityView.privacy.history                  "Ali sem to stran obiskal že kdaj pred današnjim dnem?">
<!ENTITY  securityView.privacy.cookies                  "Ali to spletno mesto shranjuje podatke (piškotke) na moj računalnik?">
<!ENTITY  securityView.privacy.viewCookies              "Preglej piškotke">
<!ENTITY  securityView.privacy.viewCookies.accessKey    "j">
<!ENTITY  securityView.privacy.passwords                "Ali sem shranil kakšno geslo za to spletno mesto?">
<!ENTITY  securityView.privacy.viewPasswords            "Preglej shranjena gesla">
<!ENTITY  securityView.privacy.viewPasswords.accessKey  "g">

<!ENTITY  securityView.technical.header                 "Tehnične podrobnosti">
