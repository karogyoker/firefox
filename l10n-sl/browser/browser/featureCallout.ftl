# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Naprej

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Razumem!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Preskakujte med napravami s sinhroniziranimi zavihki

callout-firefox-view-tab-pickup-subtitle = Hitro zajemite odprte zavihke s telefona in jih odprite neposredno tukaj.

callout-firefox-view-recently-closed-title = V trenutku si povrnite zaprte zavihke

callout-firefox-view-recently-closed-subtitle = Vsi zavihki, ki ste jih zaprli, se bodo prikazali tukaj. Nikoli več ne skrbite, da bi nenamerno zaprli spletno stran.

callout-firefox-view-colorways-title = Dodajte kanček barve

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Izberite odtenek barvne kombinacije, ki vas nagovarja. Samo v { -brand-product-name(sklon: "mestnik") }.

callout-firefox-view-colorways-reminder-title = Raziščite najnovejše barvne kombinacije

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Obarvajte svoj brskalnik s temi ikoničnimi odtenki, ki jih navdihujejo neodvisni glasovi. Samo v { -brand-product-name(sklon: "dajalnik") }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Nadgradite svoje brskanje s sinhronizacijo zavihkov

continuous-onboarding-firefox-view-tab-pickup-subtitle = Uporabite zavihke iz katere koli naprave ter sinhronizirajte svoje zaznamke, gesla in druge podatke.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Začnite
