# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Siguiente

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = ¡Entendido!

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = Cambia entre dispositivos llevándote tus pestañas

callout-firefox-view-tab-pickup-subtitle = Toma rápidamente las pestañas abiertas de tu teléfono y ábrelas para obtener un flujo de trabajo máximo.

callout-firefox-view-recently-closed-title = Recupera tus pestañas cerradas en un instante

callout-firefox-view-recently-closed-subtitle = Todas tus pestañas cerradas aparecerán mágicamente aquí. No vuelvas a preocuparte por cerrar accidentalmente una página.

callout-firefox-view-colorways-title = Agrega un toque de color

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = Selecciona el tono que te hable con combinaciones de colores. Solo en { -brand-product-name }.

callout-firefox-view-colorways-reminder-title = Explora nuestras últimas combinaciones de colores

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = Colorea tu navegador con estos tonos icónicos, inspirados en voces independientes. Solo en { -brand-product-name }.

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = Mejora tu navegación con la opción de pestaña

continuous-onboarding-firefox-view-tab-pickup-subtitle = Accede a tus pestañas abiertas desde cualquier dispositivo. Además, sincroniza tus marcadores, contraseñas y más.

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = Empezar
