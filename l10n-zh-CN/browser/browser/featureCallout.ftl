# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = 下一步

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = 知道了！

## Firefox View feature tour strings

# "Tab pickup" refers to the section in Firefox View that displays open
# tabs from other devices
callout-firefox-view-tab-pickup-title = 通过“接收标签页”功能在设备间自由切换

callout-firefox-view-tab-pickup-subtitle = 手机上打开的标签页，须臾间便可在此处浏览，切换自如。

callout-firefox-view-recently-closed-title = 快速恢复关闭的标签页

callout-firefox-view-recently-closed-subtitle = 您关闭的标签页都会神奇地出现在这里，无惧误关标签页。

callout-firefox-view-colorways-title = 增光添彩

# "Shade" refer to different color options in each colorway.
callout-firefox-view-colorways-subtitle = 选择最心动的配色，只在 { -brand-product-name }。

callout-firefox-view-colorways-reminder-title = 探索我们的新配色

# “Shades” refers to the different color options in each colorways
callout-firefox-view-colorways-reminder-subtitle = 你我皆凡人，纵横天地间。灵感来自于凡人之“声”的浏览器配色，由 { -brand-product-name } 独家提供。

## Continuous Onboarding - Firefox View: Tab pick up

# “Boost your browsing” refers to the added benefit the user receives from having
# access to the same browsing experience when moving from one browser to another.
# Alternative: ”Improve your browsing experience with tab pickup”
continuous-onboarding-firefox-view-tab-pickup-title = 通过“接收标签页”功能，享受无缝浏览体验

continuous-onboarding-firefox-view-tab-pickup-subtitle = 从任意设备访问已打开的标签页，还可同步书签、密码等信息。

continuous-onboarding-firefox-view-tab-pickup-primary-button-label = 开始使用
