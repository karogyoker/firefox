# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Vidèo incrustada

pictureinpicture-pause =
    .aria-label = Pausa
pictureinpicture-play =
    .aria-label = Legir

pictureinpicture-mute =
    .aria-label = Silenciós
pictureinpicture-unmute =
    .aria-label = Sonòr

pictureinpicture-unpip =
    .aria-label = Renviar a l’onglet

pictureinpicture-close =
    .aria-label = Tampar

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pausa
    .title = Pausa (barra d’espaci)
pictureinpicture-play-cmd =
    .aria-label = Legir
    .title = Legir (barra d’espaci)

pictureinpicture-mute-cmd =
    .aria-label = Amudir
    .title = Amudir ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Restablir lo son
    .title = Restablir lo son ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Renviar a l’onglet
    .title = Tornar a l’onglet

pictureinpicture-close-cmd =
    .aria-label = Tampar
    .title = Tampar ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Sostitoles
    .title = Sostitoles

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Ecran complet
    .title = Ecran complet (clic doble)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Quitar l’ecran complet
    .title = Quitar l’ecran complet (clic doble)

pictureinpicture-seekbackward-cmd =
    .aria-label = Retorn arrièr
    .title = Retorn arrièr (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Avança
    .title = Avança (→)

pictureinpicture-subtitles-label = Sostitoles

pictureinpicture-font-size-label = Talha de polissa

pictureinpicture-font-size-small = Pichona

pictureinpicture-font-size-medium = Mejana

pictureinpicture-font-size-large = Granda
