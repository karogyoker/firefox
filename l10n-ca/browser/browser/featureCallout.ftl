# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Callout dialog primary button to advance to next screen
callout-primary-advance-button-label = Següent

# Callout dialog primary button to complete the feature tour
callout-primary-complete-button-label = Entesos

## Firefox View feature tour strings

## Continuous Onboarding - Firefox View: Tab pick up

