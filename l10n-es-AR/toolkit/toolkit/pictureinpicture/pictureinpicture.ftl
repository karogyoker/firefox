# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-picture

pictureinpicture-pause =
    .aria-label = Pausa
pictureinpicture-play =
    .aria-label = Reproducir

pictureinpicture-mute =
    .aria-label = Mudo
pictureinpicture-unmute =
    .aria-label = Activar audio

pictureinpicture-unpip =
    .aria-label = Enviar de vuelta a la pestaña

pictureinpicture-close =
    .aria-label = Cerrar

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pausar
    .title = Pausar (Barra de espacios)
pictureinpicture-play-cmd =
    .aria-label = Reproducir
    .title = Reproducir (Barra de espacios)

pictureinpicture-mute-cmd =
    .aria-label = Silenciar
    .title = Silenciar ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Activar el sonido
    .title = Activar el sonido ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Enviar de vuelta a la pestaña
    .title = De vuelta a la pestaña

pictureinpicture-close-cmd =
    .aria-label = Cerrar
    .title = Cerrar ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Subtítulos
    .title = Subtítulos

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Pantalla completa
    .title = Pantalla completa (doble clic)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Salir de pantalla completa
    .title = Salir de pantalla completa (doble clic)

pictureinpicture-seekbackward-cmd =
    .aria-label = Atrás
    .title = Atrás (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Adelante
    .title = Adelante (→)

pictureinpicture-subtitles-label = Subtítulos

pictureinpicture-font-size-label = Tamaño de letra

pictureinpicture-font-size-small = Chico

pictureinpicture-font-size-medium = Mediano

pictureinpicture-font-size-large = Grande
