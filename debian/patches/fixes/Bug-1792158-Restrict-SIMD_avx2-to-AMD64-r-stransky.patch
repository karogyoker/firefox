From: Doug Thayer <dothayer@mozilla.com>
Date: Mon, 10 Oct 2022 16:15:48 +0000
Subject: Bug 1792158 - Restrict SIMD_avx2 to AMD64 r=stransky

Differential Revision: https://phabricator.services.mozilla.com/D158785
---
 mozglue/misc/SIMD.cpp      | 25 ++++++++++++++++++++++---
 mozglue/misc/SIMD_avx2.cpp |  5 ++++-
 2 files changed, 26 insertions(+), 4 deletions(-)

diff --git a/mozglue/misc/SIMD.cpp b/mozglue/misc/SIMD.cpp
index b9f01e8..3893de5 100644
--- a/mozglue/misc/SIMD.cpp
+++ b/mozglue/misc/SIMD.cpp
@@ -439,8 +439,27 @@ const char* SIMD::memchr8SSE2(const char* ptr, char value, size_t length) {
   return reinterpret_cast<const char*>(uresult);
 }
 
+// So, this is a bit awkward. It generally simplifies things if we can just
+// assume all the AVX2 code is 64-bit, so we have this preprocessor guard
+// in SIMD_avx2 over all of its actual code, and it also defines versions
+// of its endpoints that just assert false if the guard is not satisfied.
+// A 32 bit processor could implement the AVX2 instruction set though, which
+// would result in it passing the supports_avx2() check and landing in an
+// assertion failure. Accordingly, we just don't allow that to happen. We
+// are not particularly concerned about ensuring that newer 32 bit processors
+// get access to the AVX2 functions exposed here.
+#  if defined(MOZILLA_MAY_SUPPORT_AVX2) && defined(__x86_64__)
+
+bool SupportsAVX2() { return supports_avx2(); }
+
+#  else
+
+bool SupportsAVX2() { return false; }
+
+#  endif
+
 const char* SIMD::memchr8(const char* ptr, char value, size_t length) {
-  if (supports_avx2()) {
+  if (SupportsAVX2()) {
     return memchr8AVX2(ptr, value, length);
   }
   return memchr8SSE2(ptr, value, length);
@@ -453,7 +472,7 @@ const char16_t* SIMD::memchr16SSE2(const char16_t* ptr, char16_t value,
 
 const char16_t* SIMD::memchr16(const char16_t* ptr, char16_t value,
                                size_t length) {
-  if (supports_avx2()) {
+  if (SupportsAVX2()) {
     return memchr16AVX2(ptr, value, length);
   }
   return memchr16SSE2(ptr, value, length);
@@ -461,7 +480,7 @@ const char16_t* SIMD::memchr16(const char16_t* ptr, char16_t value,
 
 const uint64_t* SIMD::memchr64(const uint64_t* ptr, uint64_t value,
                                size_t length) {
-  if (supports_avx2()) {
+  if (SupportsAVX2()) {
     return memchr64AVX2(ptr, value, length);
   }
   return FindInBufferNaive<uint64_t>(ptr, value, length);
diff --git a/mozglue/misc/SIMD_avx2.cpp b/mozglue/misc/SIMD_avx2.cpp
index c518acf..a1467c7 100644
--- a/mozglue/misc/SIMD_avx2.cpp
+++ b/mozglue/misc/SIMD_avx2.cpp
@@ -8,7 +8,10 @@
 #include "mozilla/SSE.h"
 #include "mozilla/Assertions.h"
 
-#ifdef MOZILLA_MAY_SUPPORT_AVX2
+// Restricting to x86_64 simplifies things, and we're not particularly
+// worried about slightly degraded performance on 32 bit processors which
+// support AVX2, as this should be quite a minority.
+#if defined(MOZILLA_MAY_SUPPORT_AVX2) && defined(__x86_64__)
 
 #  include <cstring>
 #  include <immintrin.h>
