# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-button-firefox-view =
    .label = { -firefoxview-brand-name }
    .tooltiptext = { -firefoxview-brand-name }

menu-tools-firefox-view =
    .label = { -firefoxview-brand-name }
    .accesskey = F

firefoxview-page-title = { -firefoxview-brand-name }

firefoxview-close-button =
    .title = Cerrar
    .aria-label = Cerrar

# Used instead of the localized relative time when a timestamp is within a minute or so of now
firefoxview-just-now-timestamp = Ahora mismo

# This is a headline for an area in the product where users can resume and re-open tabs they have previously viewed on other devices.
firefoxview-tabpickup-header = Selector de pestañas
firefoxview-tabpickup-description = Abrir pestañas de otros dispositivos.

firefoxview-tabpickup-recenttabs-description = La lista de pestañas recientes iría aquí

# Variables:
#  $percentValue (Number): the percentage value for setup completion
firefoxview-tabpickup-progress-label = { $percentValue }% completado

firefoxview-tabpickup-step-signin-header = Cambie fácilmente entre dispositivos
firefoxview-tabpickup-step-signin-description = Para ver las pestañas de su teléfono aquí, primero inicie sesión o cree una cuenta.
firefoxview-tabpickup-step-signin-primarybutton = Continuar

firefoxview-tabpickup-adddevice-header = Sincronizar { -brand-product-name } en su teléfono o tableta
firefoxview-tabpickup-adddevice-description = Descargue { -brand-product-name } para dispositivos móviles e inicie sesión allí.
firefoxview-tabpickup-adddevice-learn-how = Saber cómo
firefoxview-tabpickup-adddevice-primarybutton = Obtenga { -brand-product-name } para móviles

firefoxview-tabpickup-synctabs-header = Activar la sincronización de pestañas
firefoxview-tabpickup-synctabs-description = Permitir que { -brand-short-name } comparta pestañas entre dispositivos.
firefoxview-tabpickup-synctabs-learn-how = Saber cómo
firefoxview-tabpickup-synctabs-primarybutton = Sincronizar pestañas abiertas

firefoxview-tabpickup-fxa-admin-disabled-header = Su organización ha desactivado la sincronización
firefoxview-tabpickup-fxa-admin-disabled-description = { -brand-short-name } no puede sincronizar las pestañas entre dispositivos porque su administrador desactivó la sincronización.

firefoxview-tabpickup-network-offline-header = Compruebe su conexión a internet
firefoxview-tabpickup-network-offline-description = Si está utilizando un firewall o proxy, verifique que { -brand-short-name } tenga permiso para acceder a la web.
firefoxview-tabpickup-network-offline-primarybutton = Reintentar

firefoxview-tabpickup-sync-error-header = Tenemos problemas para sincronizar
firefoxview-tabpickup-generic-sync-error-description = { -brand-short-name } no puede comunicarse con el servicio de sincronización en este momento. Vuelva a intentarlo en unos instantes.
firefoxview-tabpickup-sync-error-primarybutton = Reintentar

firefoxview-tabpickup-sync-disconnected-header = Activar la sincronización para continuar
firefoxview-tabpickup-sync-disconnected-description = Para recuperar sus pestañas, deberá permitir la sincronización en { -brand-short-name }.
firefoxview-tabpickup-sync-disconnected-primarybutton = Activar la sincronización en los ajustes

firefoxview-tabpickup-syncing = Espere mientras sus pestañas se sincronizan. Será solo un momento.

firefoxview-mobile-promo-header = Recuperar las pestañas desde su teléfono o tableta
firefoxview-mobile-promo-description = Para ver sus últimas pestañas móviles, conéctese a { -brand-product-name } en iOS o Android.
firefoxview-mobile-promo-primarybutton = Obtenga { -brand-product-name } para móviles

firefoxview-mobile-confirmation-header = 🎉 ¡Todo preparado!
firefoxview-mobile-confirmation-description = Ahora puede continuar con sus pestañas de { -brand-product-name } desde su tableta o teléfono.

firefoxview-closed-tabs-title = Cerradas recientemente

firefoxview-closed-tabs-description = Volver a abrir las páginas que cerró en este dispositivo.

firefoxview-closed-tabs-description2 = Volver a abrir las páginas que cerró en esta ventana.
firefoxview-closed-tabs-placeholder = <strong>No hay páginas cerradas recientemente</strong><br/>Nunca tema perder una pestaña cerrada por error. Siempre puede recuperarla aquí.

# refers to the last tab that was used
firefoxview-pickup-tabs-badge = Última activa

# Variables:
#   $targetURI (string) - URL that will be opened in the new tab
firefoxview-tabs-list-tab-button =
    .title = Abrir { $targetURI } en una nueva pestaña

firefoxview-try-colorways-button = Probar combinaciones de colores
firefoxview-no-current-colorway-collection = Hay nuevas combinaciones de colores en camino
firefoxview-change-colorway-button = Cambiar la combinación de colores

# Variables:
#  $intensity (String): Colorway intensity
#  $collection (String): Colorway Collection name
firefoxview-colorway-description = { $intensity } · { $collection }

firefoxview-synced-tabs-placeholder = <strong>No hay nada que ver todavía</strong><br/>La próxima vez que abra una página en { -brand-product-name } en otro dispositivo, la verá aquí como por arte de magia.

firefoxview-collapse-button-show =
    .title = Mostrar lista

firefoxview-collapse-button-hide =
    .title = Ocultar lista
