# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-Picture

pictureinpicture-pause =
    .aria-label = Pausar
pictureinpicture-play =
    .aria-label = Reproducir

pictureinpicture-mute =
    .aria-label = Silenciar
pictureinpicture-unmute =
    .aria-label = Restaurar sonido

pictureinpicture-unpip =
    .aria-label = Enviar de vuelta a la pestaña

pictureinpicture-close =
    .aria-label = Cerrar

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pausar
    .title = Pausar (barra espaciadora)
pictureinpicture-play-cmd =
    .aria-label = Reproducir
    .title = Reproducir (barra espaciadora)

pictureinpicture-mute-cmd =
    .aria-label = Silenciar
    .title = Silenciar ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Restaurar sonido
    .title = Restaurar sonido ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Enviar de vuelta a la pestaña
    .title = Vuelta a la pestaña

pictureinpicture-close-cmd =
    .aria-label = Cerrar
    .title = Cerrar ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Subtítulos
    .title = Subtítulos

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Pantalla completa
    .title = Pantalla completa (doble clic)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Salir de pantalla completa
    .title = Salir de pantalla completa (doble clic)

pictureinpicture-seekbackward-cmd =
    .aria-label = Retroceder
    .title = Retroceder (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Avanzar
    .title = Avanzar (→)

pictureinpicture-subtitles-label = Subtítulos

pictureinpicture-font-size-label = Tamaño de letra

pictureinpicture-font-size-small = Pequeño

pictureinpicture-font-size-medium = Medio

pictureinpicture-font-size-large = Grande
