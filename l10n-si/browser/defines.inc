# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#filter emptyLines

#define MOZ_LANGPACK_CREATOR mozilla.org

# If non-English locales wish to credit multiple contributors, uncomment this
# variable definition and use the format specified.
#define MOZ_LANGPACK_CONTRIBUTORS <em:contributor>හෙළබස සමූහය</em:contributor> <em:contributor>ධනිෂ්ක නවින්</em:contributor> <em:contributor>කල්ප පැතුම්</em:contributor> <em:contributor>ගයාන් කල්හාර</em:contributor> <em:contributor>රෝහන දසනායක</em:contributor> <em:contributor>ශ්‍රීෂානු ලොකුපතිරගේ</em:contributor> <em:contributor>හර්ෂණ වීරසිංහ</em:contributor> <em:contributor>ළහිරු හිමේෂ්</em:contributor> <em:contributor>නුවන් ඉන්දික</em:contributor> <em:contributor>ලොහාන් ගුණවීර</em:contributor>

#unfilter emptyLines
